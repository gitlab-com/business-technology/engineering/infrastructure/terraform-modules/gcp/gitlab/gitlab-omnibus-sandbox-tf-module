# [terraform-project]/main.tf

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.47"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.47"
    }
  }
  required_version = ">= 0.13"
}

# Define the Google Cloud Provider
provider "google" {
  credentials = file("./keys/gcp-service-account.json")
  project     = var.gcp_project
}

# Define the Google Cloud Provider with beta features
provider "google-beta" {
  credentials = file("./keys/gcp-service-account.json")
  project     = var.gcp_project
}

# Sandbox Environment with VPC, DNS, GitLab Omnibus, and CI Cluster
module "gitlab_sandbox" {
  source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gitlab/gitlab-omnibus-sandbox-tf-module.git"

  env_name          = var.env_name
  env_prefix        = var.env_prefix
  env_sandbox_mode  = true
  gcp_dns_zone_fqdn = var.gcp_dns_zone_fqdn
  gcp_project       = var.gcp_project
  gcp_region        = var.gcp_region
  gcp_region_cidr   = "10.128.0.0/12"
  gcp_region_zone   = var.gcp_region_zone

}
